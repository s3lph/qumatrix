# Qumatrix

A [uMatrix][umatrix]-like resource blocker extension for
[qutebrowser][qutebrowser].

## Installation

Add something like this to your `config.py`:

```python
import importlib
# Load and initialize the qumatrix module
m_spec = importlib.util.spec_from_file_location('qumatrix', 'qumatrix.py')
module = importlib.util.module_from_spec(m_spec)
# Pass c and config on to the module
module.config = config
module.c = c
# Actually import the qumatrix module
m_spec.loader.exec_module(module)
```

## Usage

On any page, enter `gg` to open the Qumatrix config page for the
current page.

[umatrix]: https://github.com/gorhill/uMatrix
[qutebrowser]: http://qutebrowser.org/