
import urllib.parse
import json
import enum
import time
import os

from PyQt5.QtCore import QUrl

from qutebrowser.api import cmdutils, interceptor
from qutebrowser.browser import qutescheme
from qutebrowser.utils import objreg, urlutils, jinja, log
from qutebrowser.keyinput import keyutils
from qutebrowser.extensions import interceptors
from qutebrowser.browser.webengine import webenginesettings, webenginetab


CONFIG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'qumatrix-config.json')


_QUMATRIX_RESOURCE_MAP = {
    interceptors.ResourceType.sub_frame: 'frame',
    interceptors.ResourceType.stylesheet: 'css',
    interceptors.ResourceType.script: 'script',
    interceptors.ResourceType.image: 'image',
    interceptors.ResourceType.font_resource: 'font',
    interceptors.ResourceType.sub_resource: 'other',
    interceptors.ResourceType.object: 'other',
    interceptors.ResourceType.media: 'media',
    interceptors.ResourceType.worker: 'worker',
    interceptors.ResourceType.shared_worker: 'worker',
    interceptors.ResourceType.prefetch: 'other',
    interceptors.ResourceType.favicon: 'image',
    interceptors.ResourceType.xhr: 'xhr',
    interceptors.ResourceType.ping: 'ping',
    interceptors.ResourceType.service_worker: 'worker',
    interceptors.ResourceType.csp_report: 'other',
    interceptors.ResourceType.plugin_resource: 'other',
    interceptors.ResourceType.preload_main_frame: 'frame',
    interceptors.ResourceType.preload_sub_frame: 'frame'
}


GOOD_FIRST_SCHEMES = ['qute', 'chrome', 'about', 'devtools']
GOOD_THIRD_SCHEMES = ['data', 'blob']
GOOD_RESOURCES = [
    interceptors.ResourceType.main_frame
]


_STATS = {}


def domainhierarchy(domain):
    if domain == '*' or domain == '1st party':
        return [domain]
    tokens = domain.split('.')
    hierarchy = []
    for i in range(len(tokens)-2, -1, -1):
        hierarchy.append('.'.join(tokens[i:]))
    return hierarchy


def is_subdomain(a, b):
    at = list(reversed(a.split('.')))
    bt = list(reversed(b.split('.')))
    return os.path.commonprefix([at, bt]) == bt


def domainsort(iterable, first=None):
    s = sorted(list(set(iterable)), key=lambda x: '.'.join(reversed(x.split('.'))))
    if first:
        s = sorted(s, key=lambda x: not is_subdomain(x, first))
    return s


class Ruleset:

    def __init__(self):
        self._rules = {}

    def add_rule(self, domain1, domain3, resource, path, query, permitted):
        self._rules.setdefault(domain1, {}).setdefault(domain3, {}).setdefault(resource, {}).setdefault(path, {})[query] = permitted

    def toggle_rule(self, domain1, domain3, resource, path, query, permitted):
        p = self._rules.setdefault(domain1, {}).setdefault(domain3, {}).setdefault(resource, {}).setdefault(path, {})
        if query in p and p[query] == permitted:
            del p[query]
        else:
            p[query] = permitted

    def eval(self, domain1, domain3, resource, path, query, *, strict=False):
        if strict:
            d1lookup = [domain1]
            d3lookup = [domain3]
            rlookup = [resource]
            plookup = [path]
            qlookup = [query]
        else:
            d1lookup = ['*']
            d3lookup = ['*']
            rlookup = ['*', resource]
            if domain3 == domain1:
                d3lookup.append('1st party')
            d1lookup.extend(domainhierarchy(domain1))
            d3lookup.extend(domainhierarchy(domain3))
            plookup = ['*', path]
            qlookup = ['*', query]

        permitted = None
        for d1 in d1lookup:
            if d1 not in self._rules:
                continue
            d1rules = self._rules[d1]
            for d3 in d3lookup:
                if d3 not in d1rules:
                    continue
                d3rules = d1rules[d3]
                for r in rlookup:
                    if r not in d3rules:
                        continue
                    rrules = d3rules[r]
                    for p in plookup:
                        if p not in rrules:
                            continue
                        prules = rrules[p]
                        for q in qlookup:
                            if q not in prules:
                                continue
                            permitted = prules[q]
        if not strict and permitted is None:
            return False
        return permitted

    def subruleclass(self, domain1, domain3, resource):
        base = self.eval(domain1, domain3, resource, '*', '*')
        rules = self._rules.get(domain1, {}).get(domain3, {}).get(resource, {})
        subrules = {query for path in rules.values() for query in path.values()}
        if (not base) in subrules:
            return 'subrulediff'
        return ''

    def cssclass(self, domain1, domain3, resource):
        return self.csspathclass(domain1, domain3, resource, '*', '*')

    def csspathclass(self, domain1, domain3, resource, path, query):
        cls = ''
        soft = self.eval(domain1, domain3, resource, path, query)
        hard = self.eval(domain1, domain3, resource, path, query, strict=True)
        if soft:
            cls='softgreen'
        else:
            cls='softred'
        if hard is not None:
            if hard:
                cls='hardgreen'
            else:
                cls='hardred'
        return cls

    def load(self, filename):
        try:
            with open(filename, 'r') as f:
                self._rules = json.loads(f.read())
        except:
            pass

    def save(self, filename):
        with open(filename, 'w') as f:
            f.write(json.dumps(self._rules))


RULESET = Ruleset()

RULESET.add_rule('*', '1st party', '*', '*', '*', True)
RULESET.add_rule('*', '1st party', 'script', '*', '*', False)
RULESET.add_rule('*', '1st party', 'frame', '*', '*', False)
RULESET.add_rule('*', '1st party', 'ping', '*', '*', False)
RULESET.add_rule('*', '1st party', 'worker', '*', '*', False)
RULESET.add_rule('*', '1st party', 'cookie', '*', '*', False)
RULESET.add_rule('*', '1st party', 'font', '*', '*', False)

RULESET.add_rule('*', '*', 'font', '*', '*', False)
RULESET.add_rule('*', '*', 'cookie', '*', '*', False)
RULESET.add_rule('*', '*', 'ping', '*', '*', False)


TEMPLATE = '''
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Qumatrix - {{ party1 }}</title>
    <style>
      table {
          border-collapse: collapse;
          border: 2px solid black;
      }
      tr.sep {
          border-top: 2px solid black;
      }
      td {
          border: 1px solid black;
          padding: 5px;
          min-width: 30px;
          position: relative;
      }
      .softgreen {
          background: #ddffdd;
      }
      .softred {
          background: #ffdddd;
      }
      .hardgreen {
          background: #66ff66;
      }
      .hardred {
          background: #ff6666;
      }

      .subrulediff > form:after {
          content: "";
          position: absolute;
          right: 0px;
          width: 0px;
          height: 0px;
          top: 0px;
          border-left: 10px solid transparent;
          z-index: 1;
      }
      .softred.subrulediff > form:after, .hardred.subrulediff > form:after {
          border-top: 10px solid #66ff66;
      }
      .softgreen.subrulediff > form:after, .hardgreen.subrulediff > form:after {
          border-top: 10px solid #ff6666;
      }

      td.domain {
          border-right: 2px solid black;
          text-align: right;
      }
      td.center {
          text-align: center;
      }
      td span.tooltip {
          display: none;
          position: absolute;
          z-index: 100;
          left: 100%;
          top: 0px;
          width: auto;
          height: auto;
          background: white;
          border: 1px solid black;
          text-align: left;
      }
      td span.tooltip ul {
          list-style-type: none;
          padding: 0;
      }
      td:hover span.tooltip {
          display: block;
      }
      input.qumatrix {
          position: absolute;
          left: 0px;
          right: 0px;
          width: 100%;
          height: 50%;
          opacity: 0;
          border: none;
          padding: 0px;
          z-index: 2;
      }
      input.qumatrix:hover {
          opacity: 0.5;
          cursor: pointer;
      }
      input.qumatrix.plus {
          top: 0px;
          background: #66ff66;
      }
      input.qumatrix.minus {
          bottom: 0px;
          background: #ff6666;
      }
      li.pathlistitem {
          white-space: nowrap;
          margin: 5px;
          overflow-x: hidden;
      }
      li.pathlistitem span {
          white-space: nowrap;
          overflow-x: hidden;
          position: relative;
          display: inline-block;
      }
    </style>
  </head>
  <body>
    <a id="qumatrix-save" href="?save=1">Save</a>
    <a id="qumatrix-load" href="?load=1">Restore</a>
    <table id="qumatrix">

      <thead>
        <tr>
          {%- set tokens = party1.split('.') %}
          <td class="domain {{ rules.cssclass(party1, '*', '*') }}">
            {%- for token in party1.split('.') -%}
              <a href="qute://qumatrix/{%- for i in range(loop.index0, tokens | length)-%}{{ tokens[i] }}{%- if not loop.last -%}.{%- endif -%}{%- endfor -%}">
                {{- token -}}
              </a>
              {%- if not loop.last -%}.{%- endif -%}
            {%- endfor -%}
            <form>
              <input type="hidden" name="first" value="{{ party1 }}"/>
              <input type="hidden" name="third" value="*"/>
              <input type="hidden" name="resource" value="*"/>
              <input class="qumatrix plus" type="submit" name="action" value="+"/>
              <input class="qumatrix minus" type="submit" name="action" value="-"/>
            </form>
          </td>
          {%- for r in ['cookie', 'image', 'media', 'css', 'font', 'script', 'xhr', 'frame', 'ping', 'worker', 'other'] %}
          <td class="center {{ rules.cssclass(party1, '*', r) }}">
            {{ r }}
            <form>
              <input type="hidden" name="first" value="{{ party1 }}"/>
              <input type="hidden" name="third" value="*"/>
              <input type="hidden" name="resource" value="{{ r }}"/>
              <input class="qumatrix plus" type="submit" name="action" value="+"/>
              <input class="qumatrix minus" type="submit" name="action" value="-"/>
            </form>
          </td>
          {%- endfor %}
        </tr>
      </thead>

      <tbody>

      {%- for section in [['*', '1st party'], owndomains, otherdomains] %}

        {%- for domain in section %}
        <tr class="{% if loop.first %}sep{% endif %}">
          <td class="domain {{ rules.cssclass(party1, domain, '*') }}">
            {{ domain }}
            <form>
              <input type="hidden" name="first" value="{{ party1 }}"/>
              <input type="hidden" name="third" value="{{ domain }}"/>
              <input type="hidden" name="resource" value="*"/>
              <input class="qumatrix plus" type="submit" name="action" value="+"/>
              <input class="qumatrix minus" type="submit" name="action" value="-"/>
            </form>
          </td>
          {%- for r in ['cookie', 'image', 'media', 'css', 'font', 'script', 'xhr', 'frame', 'ping', 'worker', 'other'] %}
          <td class="center {{ rules.cssclass(party1, domain, r) }} {{ rules.subruleclass(party1, domain, r) }}">
            {%- if stats.get(domain, {}).get(r, []) | length > 0 %}
            {{ stats.get(domain, {}).get(r, []) | length | default('', true) }}
            <span class="tooltip">
              <ul>
               {#- TODO: Also show resources from rules #}
               {%- for resource in stats.get(domain, {}).get(r, []) %}
               <li class="pathlistitem">
                 <span class="{{ rules.csspathclass(party1, domain, r, resource.path(), '*') }}">
                   {{ resource.path() }}
                   <form>
                     <input type="hidden" name="first" value="{{ party1 }}"/>
                     <input type="hidden" name="third" value="{{ domain }}"/>
                     <input type="hidden" name="resource" value="{{ r }}"/>
                     <input type="hidden" name="path" value="{{ resource.path() }}"/>
                     <input type="hidden" name="query" value="*"/>
                     <input class="qumatrix plus" type="submit" name="action" value="+"/>
                     <input class="qumatrix minus" type="submit" name="action" value="-"/>
                   </form>
                 </span>
                 {%- if resource.query() | default('') | length > 0 %}
                 <span class="{{ rules.csspathclass(party1, domain, r, resource.path(), resource.query()) }}">
                   ?{{ resource.query() }}
                   <form>
                     <input type="hidden" name="first" value="{{ party1 }}"/>
                     <input type="hidden" name="third" value="{{ domain }}"/>
                     <input type="hidden" name="resource" value="{{ r }}"/>
                     <input type="hidden" name="path" value="{{ resource.path() }}"/>
                     <input type="hidden" name="query" value="{{ resource.query() }}"/>
                     <input class="qumatrix plus" type="submit" name="action" value="+"/>
                     <input class="qumatrix minus" type="submit" name="action" value="-"/>
                   </form>
                 </span>
                 {%- endif %}
               </li>
               {%- endfor %}
              </ul>
            </span>
            {%- endif %}
            <form>
              <input type="hidden" name="first" value="{{ party1 }}"/>
              <input type="hidden" name="third" value="{{ domain }}"/>
              <input type="hidden" name="resource" value="{{ r }}"/>
              <input class="qumatrix plus" type="submit" name="action" value="+"/>
              <input class="qumatrix minus" type="submit" name="action" value="-"/>
            </form>
          </td>
          {%- endfor %}
        </tr>
        {%- endfor %}

      {%- endfor %}

      </tbody>

    </table>
    <script>
      function registerHandlers() {
        // Close tab on blur
        document.onvisibilitychange = function() {
          if (document.visibilityState === 'hidden') {
            window.close();
          }
        };
        document.getElementById("qumatrix-save").onclick = function() {
          xhrform({save:1});
          return false;
        };
        document.getElementById("qumatrix-load").onclick = function() {
          xhrform({load:1});
          return false;
        };
        for (let i = 0; i < document.forms.length; ++i) {
          let form = document.forms[i];
          form.onsubmit = function(e) {
            e.preventDefault();
            return false;
          }
          for (let j = 0; j < form.action.length; ++j) {
            let submit = form.action[j];
            submit.onclick = function() {
              let obj = {
                first: form.first.value,
                third: form.third.value,
                resource: form.resource.value,
                action: submit.value
              };
              if (form.hasOwnProperty("path") && form.hasOwnProperty("query")) {
                obj.path = form.path.value;
                obj.query = form.query.value;
              }
              xhrform(obj);
              return false;
            };
          }
        }
      }
      function xhrform(obj) {
        let e = encodeURIComponent
        let query = "?" + Object.keys(obj).map(k => e(k) + "=" + e(obj[k])).join("&");
        let xhr = new XMLHttpRequest();
        xhr.addEventListener("load", function(e) {
          var parser = new DOMParser();
          var doc = parser.parseFromString(xhr.responseText, 'text/html');
          document.getElementById("qumatrix").innerHTML = doc.getElementById("qumatrix").innerHTML;
          registerHandlers();
        });
        xhr.open("GET", query);
        xhr.send();
      }
      registerHandlers();
    </script>
  </body>
</html>
'''
JTEMPLATE = jinja.environment.from_string(TEMPLATE)


@cmdutils.register()
@cmdutils.argument('urlstr')
@cmdutils.argument('window_id', value=cmdutils.Value.win_id)
def qumatrix_edit(urlstr: str, window_id) -> None:
    """
    Open the Qumatrix settings for this page
    """
    urldata = QUrl(urlstr)
    url = QUrl(f'qute://qumatrix/{urldata.host()}')
    tb = objreg.get('tabbed-browser', scope='window', window=window_id)
    tb.tabopen(url)


@qutescheme.add_handler('qumatrix')
def qumatrix_page(url):
    try:
        path = url.path()[1:]
        if len(path) == 0:
            path = '*'
        stats = _STATS.get(path, {})
        if url.hasQuery():
            qargs = urllib.parse.parse_qs(url.query())
            if 'first' in qargs and 'third' in qargs and 'resource' in qargs and 'action' in qargs:
                if 'path' in qargs and 'query' in qargs:
                    RULESET.toggle_rule(qargs['first'][0], qargs['third'][0], qargs['resource'][0], qargs['path'][0], qargs['query'][0], qargs['action'][0] == '+')
                else:
                    RULESET.toggle_rule(qargs['first'][0], qargs['third'][0], qargs['resource'][0], '*', '*', qargs['action'][0] == '+')
            if 'save' in qargs:
                RULESET.save(CONFIG_FILE)
            if 'load' in qargs:
                RULESET.load(CONFIG_FILE)

        domains = []
        hier = domainhierarchy(path)
        for domain in stats.keys():
            domains.extend(domainhierarchy(domain))
        for domain in RULESET._rules.get(path, {}).keys():
            domains.extend(domainhierarchy(domain))
        domains.extend(hier)

        topdomain = hier[0] if len(hier) > 0 else '*'
        domains = domainsort(domains, first=topdomain)

        own = [d for d in domains if is_subdomain(d, topdomain)]
        other = [d for d in domains if not is_subdomain(d, topdomain)]
        return 'text/html', jinja.render(JTEMPLATE, party1=path, owndomains=own, otherdomains=other, stats=stats, rules=RULESET).encode()
    except BaseException as e:
        log.extensions.error(e)
        return 'text/plain', str(e).encode()


def qumatrix_cookie_filter(request):
    try:
        if request.firstPartyUrl.scheme() in GOOD_FIRST_SCHEMES:
            return True
        party1 = request.firstPartyUrl.host()
        party3 = request.origin.host()
        path = request.origin.path()
        query = request.origin.query()
        for party in domainhierarchy(party1):
            p1 = _STATS.setdefault(party, dict())
            p3 = p1.setdefault(party3, dict())
            r = p3.setdefault('cookie', set())
            r.add(QUrl(request.origin))

        if RULESET.eval(party1, party3, 'cookie', path, query) == False:
            log.extensions.info(f'Cookies for {party3} blocked by Qumatrix.')
            return False
        else:
            log.extensions.debug(f'Cookies for {party3} allowed by Qumatrix.')
            return True
    except:
        log.extensions.error('Something went wrong - allowing cookie.')
        return True


@interceptor.register
def qumatrix_interceptor(request):
    start = time.time()
    try:
        if request.first_party_url.scheme() in GOOD_FIRST_SCHEMES:
            return
        if request.request_url.scheme() in GOOD_THIRD_SCHEMES:
            return
        if request.resource_type in GOOD_RESOURCES:
            return
        party1 = request.first_party_url.host()
        party3 = request.request_url.host()
        resource = _QUMATRIX_RESOURCE_MAP.get(request.resource_type, 'other')
        path = request.request_url.path()
        query = request.request_url.query()
        for party in domainhierarchy(party1):
            p1 = _STATS.setdefault(party, dict())
            p3 = p1.setdefault(party3, dict())
            r = p3.setdefault(resource, set())
            r.add(QUrl(request.request_url))

        if RULESET.eval(party1, party3, resource, path, query) == False:
            request.block()
            log.extensions.info(f'{resource} request to {party3} blocked by Qumatrix.')
        else:
            log.extensions.debug(f'{resource} request to {party3} allowed by Qumatrix.')

    except:
        log.extensions.error('Something went wrong - allowing request.')
    end = time.time()
    log.extensions.debug(f'{party1} {party3} {resource} {path} {query} {end-start}')


RULESET.load(CONFIG_FILE)
config.bind('gg', 'qumatrix-edit {url}', mode='normal')

# hook webenginetab installation for cookie filter
def _hook(f):
    def __hook():
        ret = f()
        for profile in [webenginesettings.default_profile, webenginesettings.private_profile]:
            if profile:
                try:
                    cstore = profile.cookieStore()
                    cstore.setCookieFilter(qumatrix_cookie_filter)
                except:
                    log.extensions.error(f'Qumatrix Cookie filter installation failed')
        return ret
    return __hook
webenginetab.init = _hook(webenginetab.init)
